﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Queue<T>  // односвязный список
    {
        Node<T> head { get; set; } //первый элемент
        Node<T> tail { get; set; } // последний элемент
        int count;  // количество элементов в списке
 
        //вывод списка
        public string PrintNode()
        {
            string res = "";
            Node<T> temp = head;
            while (temp != null)
            {
                if (temp.Next != null)
                    res += temp.Data + " <-- ";
                else
                {
                    res += temp.Data;
                }
                temp = temp.Next;
            }
            return res;
        }
        // добавление элемента
        public void Enqueue(T data)
        {
            Node<T> node = new Node<T>(data);
 
            if (count == 0)
                head = tail = node;
            else
                tail.Next = node;
            tail = node;
 
            count++;
        }

        // удаление элемента
        public T Dequeue()
        {
            T value = head.Data;
            if(head == tail)
            {
                head = tail = null;
            }
            else
            {
                head = head.Next;
            }
            count--;
            return value;
        }
        // содержит ли список элемент
        public bool Contains(T data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            return false;
        }
        // вывод первого элемента
        public T Front()
        {
            return head.Data;
        }
        // вывод последнего элемента
        public T Back()
        {
            return tail.Data;
        }
    }
}
