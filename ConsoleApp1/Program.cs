﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Console.WriteLine(linkedList.PrintNode());
            linkedList.Dequeue();
            Console.WriteLine(linkedList.PrintNode());
            bool isPresent = linkedList.Contains("Sam");
            Console.WriteLine(isPresent == true ? "Sam присутствует" : "Sam отсутствует");

            Console.WriteLine("Первый элемент: " + linkedList.Front());
            Console.WriteLine("Последний элемент: " + linkedList.Back());
            
        }
    }
}
