using NUnit.Framework;
using ConsoleApp1;

namespace NUnitTestProject2
{
    public class Tests
    {
        [Test]
        public void TestContains()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.IsTrue(linkedList.Contains("Sam"));
            Assert.IsFalse(linkedList.Contains("Andrey"));
        }

        [Test]
        public void TestEnqueue()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.AreEqual(linkedList.PrintNode(), "Tom <-- Alice <-- Bob <-- Sam");
        }

        [Test]
        public void TestDequeue()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.AreEqual(linkedList.PrintNode(), "Tom <-- Alice <-- Bob <-- Sam");
            linkedList.Dequeue();
            Assert.AreEqual(linkedList.PrintNode(), "Alice <-- Bob <-- Sam");
            linkedList.Dequeue();
            linkedList.Dequeue();
            linkedList.Dequeue();
            Assert.AreEqual(linkedList.PrintNode(), "");
        }

        [Test]
        public void TestPrint()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.AreEqual(linkedList.PrintNode(), "Tom <-- Alice <-- Bob <-- Sam");
        }

        [Test]
        public void TestFront()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.AreEqual(linkedList.Front(), "Tom");
        }

        [Test]
        public void TestBack()
        {
            Queue<string> linkedList = new Queue<string>();
            linkedList.Enqueue("Tom");
            linkedList.Enqueue("Alice");
            linkedList.Enqueue("Bob");
            linkedList.Enqueue("Sam");
            Assert.AreEqual(linkedList.Back(), "Sam");
        }
    }
}